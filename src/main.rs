#[macro_use]
extern crate penrose;
mod hooks;
mod styles;
mod widget;

use penrose::{
    core::{
        data_types::Change::{Less, More},
        helpers::index_selectors,
        hooks::Hooks,
        layout::{side_stack, LayoutConf},
        ring::Direction::{Backward, Forward},
        Layout,
    },
    logging_error_handler, new_xcb_backed_window_manager, Config, Selector, XcbConnection,
};
use simplelog::{LevelFilter, SimpleLogger};
use styles::colors;

pub const FLOAT_CLASS: &str = "floating";

pub const TERMINAL: &str = "alacritty";
pub const SCREENSHOT: &str = "flameshot gui";
pub const CALCULATOR: &str = "gnome-calculator";
pub const LOCK: &str = "i3lock -c 000000 --show-failed-attempts --nofork";
// pub const LAUNCHER: &str = "dmenu_run";
pub const LAUNCHER: &str = "rofi -modi drun -show drun -theme $HOME/projects/lua/awesome-dotfiles/awesome/configuration/mirage/rofi.rasi";
pub const PATH_TO_START_SCRIPT: &str = "$HOME/.penrose";

fn main() -> penrose::Result<()> {
    if let Err(e) = SimpleLogger::init(LevelFilter::Info, simplelog::Config::default()) {
        panic!("unable to set log level: {}", e);
    };

    let floating_classes = vec!["rofi", "gnome-calculator", FLOAT_CLASS];

    let side_stack_layout = Layout::new("[[]=]", LayoutConf::default(), side_stack, 1, 0.6);
    let config = Config::default()
        .builder()
        .show_bar(true)
        .top_bar(true)
        .bar_height(35)
        .gap_px(1)
        .floating_classes(floating_classes)
        .layouts(vec![side_stack_layout])
        .focused_border(colors::PURPLE)?
        .build()
        .expect("Unable to build configuration");

    let key_bindings = gen_keybindings! {
        // Program launchers
        "M-p" => run_external!(LAUNCHER);
        "M-grave" => run_external!(LAUNCHER);
        "M-Return" => run_external!(TERMINAL);
        "M-S-Return" => run_external!(SCREENSHOT);
        "M-l" => run_external!(LOCK);
        "Print" => run_external!(SCREENSHOT);
        "Sys_Req" => run_external!(SCREENSHOT);
        "XF86Calculator" => run_external!(CALCULATOR);
        "XF86AudioMute" => run_external!("amixer set Master toggle");
        "XF86AudioLowerVolume" => run_external!("amixer set Master 5%-");
        "XF86AudioRaiseVolume" => run_external!("amixer set Master 5%+");

        "XF86MonBrightnessUp" => run_external!("xbacklight -inc 10");
        "XF86MonBrightnessDown"  => run_external!("xbacklight -dec 10");

        "XF86KbdBrightnessUp"  => run_external!("xbacklight -dec 10");
        "XF86KbdBrightnessDown"  => run_external!("xbacklight -dec 10");

        // Exit Penrose (important to remember this one!)
        "M-A-C-Escape" => run_internal!(exit);

        // client management
        "M-j" => run_internal!(cycle_client, Forward);
        "M-k" => run_internal!(cycle_client, Backward);
        "M-S-j" => run_internal!(drag_client, Forward);
        "M-S-k" => run_internal!(drag_client, Backward);
        "M-f" => run_internal!(toggle_client_fullscreen, &Selector::Focused);
        "M-q" => run_internal!(kill_client);

        // workspace management
        "M-Tab" => run_internal!(toggle_workspace);
        "M-A-period" => run_internal!(cycle_workspace, Forward);
        "M-A-comma" => run_internal!(cycle_workspace, Backward);

        // Layout management
        // "M-grave" => run_internal!(cycle_layout, Forward);
        "M-S-grave" => run_internal!(cycle_layout, Backward);
        "M-A-Up" => run_internal!(update_max_main, More);
        "M-A-Down" => run_internal!(update_max_main, Less);
        // "M-l" => run_internal!(update_main_ratio, More);
        // "M-h" => run_internal!(update_main_ratio, Less);
        "M-C-Right" => run_internal!(update_main_ratio, More);
        "M-C-Left" => run_internal!(update_main_ratio, Less);

        map: { "1", "2", "3", "4", "5", "6", "7", "8", "9" } to index_selectors(9) => {
             "M-{}" => focus_workspace (REF);
             "M-S-{}" => client_to_workspace (REF);
         };
    };

    let hooks: Hooks<XcbConnection> =
        vec![Box::new(hooks::StartupScript::new(PATH_TO_START_SCRIPT))];

    let mut wm = new_xcb_backed_window_manager(config, hooks, logging_error_handler())?;
    wm.grab_keys_and_run(key_bindings, map! {})
}
