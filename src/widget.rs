use penrose::core::{
    hooks::Hook,
    manager::WindowManager,
    xconnection::{XConn, Xid},
};
use penrose::draw::{Color, DrawContext, Result, Widget};

pub struct DemoWidget {}

impl DemoWidget {
    pub fn new() -> Self {
        Self {}
    }
}

impl<X> Hook<X> for DemoWidget
where
    X: XConn,
{
    fn client_name_updated(
        &mut self,
        _: &mut WindowManager<X>,
        _: Xid,
        _name: &str,
        _is_root: bool,
    ) -> penrose::Result<()> {
        // if is_root {
        //     self.txt.set_text(name);
        // }

        Ok(())
    }
}

impl Widget for DemoWidget {
    fn draw(
        &mut self,
        ctx: &mut dyn DrawContext,
        _s: usize,
        _f: bool,
        w: f64,
        h: f64,
    ) -> Result<()> {
        ctx.color(&mut Color::new_from_hex(0x115599));
        ctx.rectangle(0.0, 0.0, 11.0, 34.0);
        ctx.rectangle(0.0, 0.0, w, h);
        ctx.rectangle(0.0, 0.0, w - 1.0, h - 1.0);
        Ok(())
    }

    fn current_extent(&mut self, _ctx: &mut dyn DrawContext, h: f64) -> Result<(f64, f64)> {
        Ok((100.0, h))
    }

    fn require_draw(&self) -> bool {
        true
    }

    fn is_greedy(&self) -> bool {
        false
    }
}
